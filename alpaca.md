# Alpaca, � ��� � ��� ����

## ��� ����������

[��������� ASCOM]
https://github.com/ASCOMInitiative/ASCOMPlatform/releases/download/v6.5SP1Release/ASCOMPlatform65SP1.exe

[���������� ������������]
https://github.com/ASCOMInitiative/ASCOMPlatform/releases/download/v6.5SP1Release/ASCOMPlatform65SP1Developer.exe

[ASCOM Remote, ������ Alpaca]
https://github.com/ASCOMInitiative/ASCOMRemote/releases

## ��������

[�������� ����������� ASCOM]
ASCOM Developer Help ��������������� ������ � ������������ ������������.
��� ������ ��������� ������, ASCOM Namespaces -> ASCOM.DeviceInterface

[�������� Alpaca API]
https://ascom-standards.org/Developer/ASCOM%20Alpaca%20API%20Reference.pdf

[Swagger - ����� �����������, ��� ��� API ��������]
https://ascom-standards.org/api/

## ��� ��� ������ ������ ��������

������ Alpaca ��� ���-������, ������� ��������� ������� �� �����������
������� � ����� JSON.

������� ������ ���� �����:  

1. `GET /api/v1/telescope/0/rightascension?ClientID=323&ClientTransactionID=456`  
�����: `{"value":9.921671522284548,"clientTransactionID":456,"serverTransactionID":789,"errorNumber":0,"errorMessage":""}`    

2. `PUT /api/v1/telescope/0/slewtocoordinatesasync`  
� ���� �������: `ClientID=323&ClientTransactionID=456&RightAscension=2.14&Declination=45.3`  
�����: `{"clientTransactionID":456,"serverTransactionID":323,"errorNumber":1025,"errorMessage":"Point at (0.08916666666666667, 0.12583333333333332) on sky, (0.32509906969136665, 0.12583333333333332) on earth is inaccessible!"}`  
���� ������ ���, errorNumber=0 � errorMessage=������ ������

�������� ���� ������� ��. � ASCOM Developer Help, ��� 1:1 ������������� � Alpaca

������� ������, ������ ������ �������� ��������������� ������� ��������.  
���� �-��� ���������� ��������, ��� ������������ � Value ��������� JSON  
���� �� ����������, Value ����.

���� �������� (��� ��������� �����, �������� �� ������ ���������):
��������� ������� PUT ���������� � ����. 
���� �������� �� � ������ ����������� ASP.NET Core, 
�� ����� ������ ������� [FromForm] � ������� ���������
��� ���� (!!!) ASP.NET ����������� ���������, ��������� ������� ������.
������� `Declination=1.23` � ������� ������� ����������, 
�.�. � ������� ������ ������ ���� �������!
�������������� ������ ��� ������������� �������.

## ��� ������������ 

1. ������ �������, ������� ����� ����� ������� � ��������� ������

2. ����� javascript�� �� ���������  
������ � �������� ��������� ��� ��������, ��. `test.html` � `alpaca-request.js`  
����� ���:

``` javascript
function slewToCoordinates(Ra, Dec) {
    var command_executor = new AlpacaRequest("/api/v1/telescope/0");
    command_executor.command("slewtocoordinates", "RightAscension="+Ra, "Declination="+Dec);
}
```
��������� ������, ���� �����, ������� � onload:
``` javascript
    command_executor.onload = function(a) {
        cmd_status = a.response;
        fDisplayUpdate = true;
    }
   ..........
    if (cmd_status.errorNumber != 0) {
        document.getElementById("l_cmd_status").textContent = cmd_status.errorMessage;
    } else {
        document.getElementById("l_cmd_status").textContent = "Ok";
    }
```

## ��� �������

1. ���������� ����� �� ����� ���������.
��� � ��������� ���� ������ ����������.

2. ��������� ASCOM Remote Server.  
������ ���������, ����� ������ ���������.
�������� � �������� ��������� ����������.  
��������� ���������.

3. ������ ������ ������ ������� �� `localhost:11111` � �������� ��� �� ����� �������.  
� ���������, ��������� ���������� ����� ������� �� ���������, �.�. ��� �������������
� ���������� ��� �������.
��� ����� � ���� ���� ��������, �� ��� ���� ����� ��������� ������� �������.
��� ��������� ��������� ��� � ������ ��� ������� ���������� �� ����, 
�� ������ ����������� ������.
�� � ������� � ��������� ������� ��������� �������. ���� �� �������� ������������.

4. ��� ��������� ����� ��������� Cartes du Ciel, ���������� ���
� ������������ ��������� � ���������� ��� �� ����� � ��� ��������.

function svg(w, h, scale, offset, text) {
	let s = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="'+w+'" height="'+h+'">';
	s = s + '<g transform="scale('+scale+', '+scale+') translate('+offset+', '+offset+')">';
	s = s + text;
	s = s + '</g></svg>';
	return s;
}

function displayMoon(time) {
	if (time==null) time = getCurrentDate();
	let y = time.getUTCFullYear();
	let m = time.getUTCMonth()+1;
	let d = time.getUTCDate();
	let h = time.getUTCHours();
	return svg(50, 50, 50, 0.5, createMoon(MoonPhase(y,m,d,h)));
}

function moonCoords(time) {
	if (time==null) time = getCurrentDate();
	let y = time.getUTCFullYear();
	let m = time.getUTCMonth()+1;
	let d = time.getUTCDate();
	let h = time.getUTCHours();
	let moon_pos = MoonPos(y,m,d,h);
	moon_pos[0] /= 24.0; moon_pos[1] /= 360.0;
	return moon_pos;
}

// Однако, функции в JSON не попадают, 
// совсем, даже в виде результата, 
// даже сами поля отсутствуют
// и из JSON не читаются
let skyObjects = [ { 
	name: "Луна", 
	position: moonCoords,
	type: "planet", // Это признак того, что координаты и рисунок зависят от времени
	id: 1, // Это индекс в массиве функций вычисления координат и генерации рисунка
	display: true,
	image: displayMoon }, 
{
	name: "Солнце", 
	position: { ra: -0.05, dec: 0.02 },
	type: "planet", // впрочим, думаю что такие объекты надо вообще отдельно хранить и рисовать.
	id: 0,	// Например, координаты и изображение считать на сервере и отдавать готовый json и svg
	display: true,
	image: "Sun_1.svg" },
{
	name: "",
	position: { ra: 0.01, dec: -0.05 },
	type: "star",
	display: true,
	image: -1.0 },
{
	name: "",
	position: { ra: 0.02, dec: -0.05 },
	type: "star",
	display: true,
	image: {type: "star", value: 10, color: "#37F"} },
{
	name: "M31",
	position: { ra: 0.02, dec: -0.05 },
	type: "galaxy",
	display: true,
	image: {type: "galaxy", value: 3.6, size: [0.01, 0.03], orientation: 0.33, color: "#F70"} },
{
	name: "M32",
	position: { ra: 0.02, dec: -0.05 },
	type: "galaxy",
	display: true,
	image: function(){return createGalaxy(3.6, [0.007, 0.012], 0.6, "#F70")} },
{
	name: "Vert",
	position: { ra: 0.02, dec: -0.05 },
	type: "galaxy",
	display: true,
	image: {type: "galaxy", value: 3.6, size: [0.025, 0.03], orientation: 0.73, color: "#F70"} },
];



var currentDate = null;
// Может быть получена через API в будущем
// Как и эфемериды
const observatoryData = {
	code: "095",
	name: "CRAO/Nauchniy",
	lat: 34.01388,
	lon: 44.72777,
	elev: 600,
	tz: 3 
};
const siderealDaySec = 86164.0905309;
const siderealSpeed = 86400*15/siderealDaySec; //15.041084;


function getCurrentDate() {
	if (currentDate == null) return new Date();
	else return currentDate;
}

function setCurrentDate(d) {
	currentDate = d;
}

function createMoon(phase) {
	let area = 0.0;
	let s = '<g>';//'<g transform="scale(0.03, 0.03)">';
	s += '<ellipse cx="0.0" cy="0.0" rx="0.5" ry="0.5" fill="#333" stroke="none" stroke-width="0.01" />';
	s += '<path fill="#AAA" stroke="none" stroke-width="0.01" d="M 0,0.5 ';
	if (phase < 0.25) {
		s += ' A '+(0.5-phase*2)+' 0.5 180 1 1 0.0,-0.5';
		s += ' A 0.5 0.5 180 1 0 0.0,0.5" />';
		area = phase*2;
	} else if (phase < 0.50) {
		s += ' A '+(phase*2-0.5)+' 0.5 180 1 0 0.0,-0.5';
		s += ' A 0.5 0.5 180 1 0 0.0,0.5" />';
		area = phase*2;
	} else if (phase < 0.750) {
		s += ' A '+(phase*2-1.5)+' 0.5 180 1 1 0.0,-0.5';
		s += ' A 0.5 0.5 180 1 1 0.0,0.5" />';
		area = 2.0-phase*2;
	} else {
		s += ' A '+(phase*2-1.5)+' 0.5 180 1 0 0.0,-0.5';
		s += ' A 0.5 0.5 180 1 1 0.0,0.5" />';
		area = 2.0-phase*2;
	}
	s += '<text x="0.0" y="0.2" fill="#FFF" font-size="0.55" text-anchor="middle"><tspan>';
	s += (area*100).toFixed(0) + '%';
	//s += '99%';
	s += '</tspan></text>';
	s += '</g>';
	return s;
}

function createStar(v, time, color) {
	if (v>15) v=15;
	v = (17-v) / 35.0;
	if (color==null) color="#FFF";
	s = '<g>';
	s += '<circle cx="0" cy="0" r="'+v+'" fill="'+color+'" stroke="none" />';
	s += '</g>';
	return svg(50, 50, 50, 0.5, s);
}

function createLine(beg, end, color) {
	s = '<g>';
	s += '<path stroke="'+color+'" fill="none" d="M'+beg.ra+' '+beg.dec+' L'+end.ra+' '+end.dec+'" />';
	s += '</g>';
	return svg(50, 50, 50, 0.5, s);
}

function createGalaxy(v, size, orientation, color) {
	ang = orientation*2.0*3.141592653;
	dx = size[0]/2.0*Math.cos(ang);
	dy = size[0]/2.0*Math.sin(ang);
	scale = 1.0/Math.sqrt(size[0]*size[0]+size[1]*size[1]);
	s = '<g transform="scale('+scale+', '+scale+')">';
	s += '<path stroke="#073" stroke-width="0.001" fill="'+color+'" '
		+'d="M'+dx+' '+dy
		+' A'+size[0]/2.0+' '+size[1]/2.0+' '+orientation*360+' 1 1 '+(-dx)+' '+(-dy)
		+' A'+size[0]/2.0+' '+size[1]/2.0+' '+orientation*360+' 1 1 '+dx+' '+dy
		+'" />';
	s += '</g>';
	return svg(50, 50, 50, 0.5, s);
}

function createObject(o, time) {
	switch(o.type) {
		case "star": return createStar(o.value, time, o.color);
		case "line": return createLine(o.begin, o.end, o.color);
		case "galaxy": return createGalaxy(o.value, o.size, o.orientation, o.color);
	}
}

function displayStarmap() {
	tab = document.getElementById("sky_table");
	let now = getCurrentDate();
	for (i in skyObjects) {
		try {
		let obj = skyObjects[i];
		let pos = null;
		let sp = "";
		let si = "";
		switch (typeof(obj.position)) { // {ra, dec} или {ha, dec} или функция, которая возвращает такую структуру
			case "object": pos = obj.position; break;
			case "function": pos = obj.position(now); break;
		}
		if (pos) sp = typeof(obj.position)+":"+JSON.stringify(pos); else sp = typeof(obj.position)+"{ra:?, dec:?}";
		switch (typeof(obj.image)) {
			case "string": si='<img width="50px" height="50px" src="'+obj.image+'" />'; break;// url файла-изображения
			case "number": si=createStar(obj.image, now); break;// звёздная величина
			case "object": si=createObject(obj.image, now); break;// звезда подробнее, линия, текст...
			case "function": si=obj.image(now); break;// Функция отрисовки, возвращает строку - svg
			default: si=typeof(obj.image);
		}
		try {
			row = tab.rows[ Number(i)+1 ];
			if (!row) row = tab.insertRow();
		} catch {
			row = tab.insertRow();
		}
		row.cells[0].innerText = i.toString();
		row.cells[1].innerText = obj.name;
		row.cells[2].innerText = obj.display;
		row.cells[3].innerText = sp;
		row.cells[4].innerHTML = si;
		} catch (e) { 
			logException(e);
			if (row && row.cells[4]) row.cells[4].innerText = e.message;
		}
	}
	console.log(JSON.stringify(skyObjects));
}

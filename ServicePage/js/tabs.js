const buttonsNavigation = document.querySelector('.control__nav');
const buttonsTab = Array.from(buttonsNavigation.querySelectorAll('.control__title')); //массив вкладок
const tabContent = Array.from(document.querySelectorAll('.main-control')); //массив контента вкладок

//переключение контента вкладок
function toggleTab() {
    //tabContent.forEach((tabElement) => {
    for (var t in tabContent) {
	var tabElement = tabContent[t];
        if (tabElement.className.includes('main-control_hide')) {
            tabElement.classList.remove('main-control_hide');
        } else {
            tabElement.classList.add('main-control_hide');
        }
    } //)
}

//закрыть вкладку
function closedTab(tabActive) {
    tabActive.classList.remove('active');
    tabActive.removeAttribute('disable', false);
    tabActive.addEventListener('click', toggleTabs);
    // toggleTab();
}

//открыть вкладку
function openedTab(tab) {
    tab.classList.add('active');
    tab.setAttribute('disable', true);
    tab.removeEventListener('click', toggleTabs);
    toggleTab();
}

//переключение вкладок
function toggleTabs(e) {
    const tab = e.target;
    const tabActive = buttonsNavigation.querySelector('.active');
    closedTab(tabActive);
    openedTab(tab);
}

buttonsTab[1].addEventListener('click', toggleTabs);
tabContent[1].classList.add('main-control_hide');
buttonsNavigation.querySelector('.control__title').classList.add('active');
buttonsNavigation.querySelector('.control__title').setAttribute('disable', true);

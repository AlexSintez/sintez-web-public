// Имя картинки с камеры
let cameraImage = "01.jpg";
const cameras = [
	{ number: 0, path: "01-2.jpg", name: "КАМ1", label: "l_cam1" },
	{ number: 1, path: "02-2.jpg", name: "КАМ2", label: "l_cam2" },
	{ number: 2, path: "101-2.jpg", name: "КАМ3", label: "l_cam3" },
	{ number: 3, path: "103-2.jpg", name: "Восток", label: "l_cam_east" },
	{ number: 4, path: "07-2.jpg", name: "Запад", label: "l_cam_west" },
	{ number: 5, path: "allsky.jpg", name: "ВсёНебо", label: "l_cam_allsky" }
];


// Обработчк 5сек таймера - тут картинку перерисовываем
function cameraUpdate() {
	setElemProperty("img01", "src", cameraImage+"?"+new Date().getTime());
}

function cameraChange(num) {
	try {
		cameraImage = cameras[num].path;
		for(let c in cameras) {
			l = document.getElementById(cameras[c].label);
			if (l != null) {
				l.textContent = cameras[c].name;
				if (cameras[c].number == num) {
					l.classList.add("active");
				} else {
					l.classList.remove("active");
				}
			}
		}
		//getDocumentById("l_cam_name").textContent = cameras[num].name;
		cameraUpdate();
	} catch (e) { logException(e); }
}


setInterval(cameraUpdate, 5000);


const ipRelayUrl = "http://192.168.88.11:5001/service/v1/iprelay/0"
//const ipRelayUrl = "http://localhost:5001/service/iprelay"

function onRelayStateReceived(st) {
	if (st != null && (st.errorNumber==null||st.errorNumber == 0)) {
		for (i in st.relay_state) {
			n = parseInt(i)+1
			name = "b_relay"+n
			if (st.relay_state[i] == 1) {
				setElemColors(name+"on", "ok");
				setElemColors(name+"off", null);
			} else if (st.relay_state[i] == 0) {
				setElemColors(name+"on", null);
				setElemColors(name+"off", "ok");
			} else {
				setElemColors(name+"on", "error");
				setElemColors(name+"off", "error");
			}
		}
	}
}

function ipRelaySet(n, v) {
	let request = new AlpacaRequest(ipRelayUrl);
	request.timeout = 2000;
	request.command("set", "&relay"+n+"="+v);
}

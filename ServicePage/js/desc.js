
function onDescriptionReceived(st) {
    try {
        if (st != null && st.value != null) {
            //if (st.value.manufacturerVersion == "version unknown") st.value.manufacturerVersion = "1.0-251";
            if (st.value.manufacturerVersion == "version unknown") st.value.manufacturerVersion = "";
            s = st.value.serverName + " " + st.value.manufacturerVersion;
            //s = "Alpaca/INDI ASP.NET 1.1-272";
            setElemProperty("l_server_desc", "innerText", s);
        }
    } catch (e) { logException(e); }
}

function updateServerStatus() {
    // Сервер
    try {
	serverLink = "";
	sl = 0.5;
	if (serverSendCount > 2) sl = serverSuccessCount / serverSendCount;
	if (serverSendCount > 50) {
	    serverLink = (serverSuccessCount / serverSendCount).toFixed(3);
	} else {
	    serverLink = serverSuccessCount + "/" + serverSendCount;
	}
	let l = document.getElementById("l_server_link");
	l.textContent = serverLink;
	setElemColors(l, sl);
    } catch (e) { console.log(e) }

    if (status.errorNumber != null && status.errorNumber != 0) {
        setElemText("l_cmd_status", status.errorMessage);
    } else {
        //document.getElementById("l_cmd_status").textContent = "Ok";
    }
    /*try {
    // Вывод сообщения об ошибке от сервера или собственного(если 500 получил или таймаут)
        if (roof_cmd_status.errorNumber != null && roof_cmd_status.errorNumber != 0) {
            document.getElementById("l_cmd_status").textContent = roof_cmd_status.errorMessage;
        } else if (roof_cmd_status.errorMessage != null && roof_cmd_status.errorMessage != "" ) {
            document.getElementById("l_cmd_status").textContent = roof_cmd_status.errorMessage;
        } else {
            if (status.errorNumber != null && status.errorNumber != 0) {
                document.getElementById("l_cmd_status").textContent = status.errorMessage;
            } else if (status.errorMessage != null && status.errorMessage != "" ) {
                document.getElementById("l_cmd_status").textContent = status.errorMessage;
            } else {
                document.getElementById("l_cmd_status").textContent = "Ok";
            }
//	document.getElementById("l_cmd_status").textContent = "Ok";
        }
    } catch(e) { console.log(e) }*/

}


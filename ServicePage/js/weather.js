/*    if temper_delta <= 5:
	current_cond = '<span class="text-danger">worst</span>' + delta_text
    else:
	if temper_delta >= 5 and temper_delta <= 11:
	    current_cond = '<span class="text-danger">very bad</span>' + delta_text
	elif temper_delta > 11 and temper_delta <= 16:
	    current_cond = '<span class="text-danger">bad</span>' + delta_text
	elif temper_delta > 16 and temper_delta <= 19:
	    current_cond = '<span class="text-warning">normal</span>' + delta_text
	elif temper_delta > 19 and temper_delta <= 22:
	    current_cond = '<span class="text-info">quite good</span>' + delta_text
	elif temper_delta > 22 and temper_delta <= 25:
	    current_cond = '<span class="text-info">good</span>' + delta_text
	elif temper_delta > 25 and temper_delta <= 35:
	    current_cond = '<span class="text-success">best</span>' + delta_text
	elif temper_delta > 35:
	    current_cond = '<span class="text-success">best of the best</span>' + delta_text
*/

function weatherSkyState(tair, tsky) {
    s = null; //"Непонятно";
    if (tair == null || tsky == null) s = "Нет данных";
    else try {
        dt = tair - tsky;
        if (dt < 5) s = "Ужасная";
        else if (dt <= 11) s = "Мерзкая";
        else if (dt <= 16) s = "Плохая";
        else if (dt < 19) s = "Приемлимо";
        else if (dt < 25) s = "Хорошая";
        else if (dt < 50) s = "Прекрасная";
    } catch (e) {
	s = ""+e;
    }
    return s;
}

function onWeatherReceived(st) {
    try { // Погода
        let s = null;
	if (st==null) return;
        try {
            s = weatherSkyState(st.air_temp, st.sky_temp);
        } catch(e) {
            logException(e);
        }
        if (s==null) {
            onWeatherError(st);
        } else {
            setElemText("l_weather_air_temp", st.air_temp.toFixed(1));
            setElemText("l_weather_air_humidity", st.air_humid.toFixed(0));
            setElemText("l_weather_sky_state", s);
            setElemText("l_weather_sky_temp",  st.sky_temp.toFixed(1));
            setElemText("l_weather_sky_quality", st.sky_quality.toFixed(1));
            //setBackground("block_weather", null);
            setStyleEnabled("block_weather");
            setStyleEnabled("block_weather_status");
        }
    } catch (e) {
        logException(e);
    }
}

function onWeatherError(st) {
    setStyleDisabled("block_weather");
    setStyleDisabled("block_weather_status");
}

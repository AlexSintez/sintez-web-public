
function AlpacaRequest(base_url) {
	this.baseUrl = base_url;
	this.fReady = false;
	this.requestStr = null;
	this.request = null;
	this.responseCode = 0;
	this.response = null;
	this.fProcessing = false;
	this.onload = null;
	this.logRequests = false;
	this.timeout = 1000;

	let self = this;

	this.abort = function() {
		try {
		if (self.request != null) { // TODO: && request.??? - проверка что запрос выполняется и есть что отменять
			self.request.abort();
			if (self.request.onloadend != null && self.request.onloadend != undefined) {
				self.request.onloadend();
			}
		}
		self.fProcessing = false;
		} catch (e) {}
	}

	this.command = function (cmd, par1, par2, par3, par4) {
		try {
		self.fReady = false;
		self.responseCode = 0;
		self.response = null;
		let request = new XMLHttpRequest();
		request.open('PUT', self.baseUrl + '/' + cmd);
		request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		request.responseType = 'text';
		request.timeout = self.timeout;
		request.onloadend = function() {
			let status;
			try {
				try {
					status = self.request.status; 
					self.responseCode = status;
				} catch (e) {
					self.responseCode = -1;
				}
				if (status==null || status==0) {
					self.response = { errorNumber: 9998, errorMessage: "No answer" };
				} else if (status>=200 && status <= 299) {
					try {
						self.response = JSON.parse(self.request.responseText);
					} catch (e) { // Не JSON, что поделать.
						self.response = { errorNumber: 0, errorMessage: self.request.responseText };
					}
				} else if (status>=300 && status <= 399) {
					// TODO: Надо обработать redirect
					self.response = { errorNumber: status, errorMessage: self.request.statusText+" "+self.request.responseText };
				} else {
					self.response =  { errorNumber: status, errorMessage: self.request.statusText+" "+self.request.responseText };
				}
			} catch (e) {
				self.response = { errorNumber: 9999, errorMessage: e};
				self.responseCode = 9999;
			}
			if (self.logRequests) console.log("command: RX:"+JSON.stringify(self.response));
			self.fProcessing = false;
			self.fReady = true;
			if (self.onload != null) self.onload(self);
		};
		let s = "";
		if (par1 != null && par1 != "") s = par1;
		if (par2 != null && par2 != "") s = s+"&"+par2;
		if (par3 != null && par3 != "") s = s+"&"+par3;
		if (par4 != null && par4 != "") s = s+"&"+par4;
		if (self.logRequests) console.log("command: PUT "+self.baseUrl + "/" + cmd+"\n"+s);
		self.requestStr ="PUT "+self.baseUrl + "/" + cmd+" / "+s;
		self.fProcessing = true;
		self.request = request;
		if (s!="") self.request.send(s);
		else self.request.send();
		} catch (e) {
			self.response = { errorNumber: 9999, errorMessage: e};
		}
	}
	this.getValue = function(cmd, par1, par2, par3, par4) {
		try {
		self.fReady = false;
		self.responseCode = 0;
		self.response = null;
		let s = "";
		if (par1 != null && par1 != "") s = par1;
		if (par2 != null && par2 != "") s = s+"&"+par2;
		if (par3 != null && par3 != "") s = s+"&"+par3;
		if (par4 != null && par4 != "") s = s+"&"+par4;
		let request = new XMLHttpRequest();
		if (s!="") s = self.baseUrl + '/' + cmd + "?" + s;
		else s = self.baseUrl + '/' + cmd;
		if (self.logRequests) console.log("getValue: GET "+s);
		self.requestStr ="GET "+s;
		request.open('GET', s);
		request.timeout = self.timeout;
		//request.responseType = 'text';
		request.onloadend = function() {
			try {
				let status = null;
				try {
					status = self.request.status; 
					self.responseCode = status;
				} catch (e) {
					self.responseCode = -1;
				}
				if (status==null || status==0) {
					self.response = { errorNumber: 9998, errorMessage: "No answer" };
				} else if (status>=200 && status <= 299) {
					try {
						self.response = JSON.parse(self.request.responseText);
					} catch (e) { // Не JSON, что поделать.
						self.response = { errorNumber: 0, errorMessage: self.request.responseText };
					}
				} else if (status>=300 && status <= 399) {
					// TODO: Надо обработать redirect
					self.response = { errorNumber: status, errorMessage: self.request.statusText+" "+self.request.responseText  };
				} else {
					self.response = { errorNumber: status, errorMessage: self.request.statusText+" "+self.request.responseText  };
				}
			} catch (e) {
				self.response = { errorNumber: 9999, errorMessage: e};
			}
			if (self.logRequests) console.log("getValue: RX:"+JSON.stringify(self.response));
			self.fProcessing = false;
			self.fReady = true;
			if (self.onload != null) self.onload(self);
		};
		self.fProcessing = true;
		self.request = request;
		self.request.send();
		} catch (e) {
			self.response = { errorNumber: 9999, errorMessage: e};
		}
	}
}



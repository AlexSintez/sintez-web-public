// Обработчик веб-интерйеса крыши. Тут всё.
// Отсылка команд (по нажатию кнопок) через roofCommand и roofSerivceCommand, К счастью, они не имеют параметров
// С телескопом не так, но тут телескоп только читаем.


///<desc>Возвращает текст по коду состояния ASCOM.IDomeV2.ShutterStatus</desc>
function roofStatus(v) {
    const rooftext = [
        "Открыто",
        "Закрыто",
        "Открывается",
        "Закрывается",
        "Ошибка",
        "Неизвестно"
    ];
    if (v < 0) v = 0;
    if (v >= rooftext.length) v = rooftext.length;
    return rooftext[v] + " (" + v + ")";
}

function onRoofStatusReceived(st) { // Принят статус alpaca (открыто, закрыто и тд)
	// Не используется, вся информация есть в status_ext
}

///TODO: Обновлять элемент только если его состояние изменилось. Нужно хранить предыдущее.
function onRoofExtStatusReceived(st) { // Принят расширенный статус
    // Крыша
    // Версия контроллера, связь, текст состояния
    if (st != null && (st.errorNumber==null||st.errorNumber == 0)) {
        try {
		setElemText("l_controller", "Овен v" + st.versionText);
		if (!st.connected || st.linkQuality<=0.001) {
		    setElemText("l_state", "Нет связи");
		    setElemText("l_controller", "");//Овен: Нет связи";
		    setElemColors("l_state", 0.0);
		    setElemText("l_roof_link", "Нет связи");
		    setElemColors("l_roof_link", 0.0);
		} else {
		    setElemText("l_state", st.statusText + st.extStatusText);
		    setElemColors("l_state", null);
		    setElemText("l_roof_link", st.linkQuality.toFixed(3));
		    setElemColors("l_roof_link", st.linkQuality);
		}
        } catch (e) { }
	setStyleEnabled("block_dome");
	setStyleEnabled("block_dome_status");
        try {
		setElemText("l_inputs", "0x"+st.inputs.toString(16));
		setElemText("l_outputs", "0x"+st.outputs.toString(16));
		setElemText("l_registers", "0x"+st.status.toString(16)+" 0x"+st.extStatus.toString(16));
        } catch (e) { }
        // Состояние крыши (открыта, закрыта, незакрыта, расцветка кнопок)
        try {
		if (st.isOpening) {
	    		setElemColors("b_openall", "warning");
	    		setElemColors("b_openroof", "warning");
	    		if (!st.isClosed) {
				setElemColors("b_closeall", null);
				setElemColors("b_closeroof", null);
			}
		}
		if (st.isClosing) {
		    setElemColors("b_closeall", "warning");
		    setElemColors("b_closeroof", "warning");
		    if (!st.isOpened) {
			setElemColors("b_openall", null);
			setElemColors("b_openroof", null);
		    }
		}
		if (st.isOpened) {
		    setElemColors("b_openall", "ok");
		    setElemColors("b_openroof", "ok");
		    setElemColors("b_closeall", "");
		    setElemColors("b_closeroof", "");
		}
		if (st.isClosed) {
		    setElemColors("b_closeall", "ok"); //ok");
		    setElemColors("b_closeroof", "ok"); //ok");
		    setElemColors("b_openall", "");
		    setElemColors("b_openroof", "");
		}
		if (st.isTelescopeParked) {
		    setElemColors("b_parkall", "ok");
		} else if (st.isParkingCommand) {
		    setElemColors("b_parkall", "warning");
		} else {
		    setElemColors("b_parkall", "");
		}
	// Питание монтировки
		if (st.isPowerOn) {
		    setElemColors("b_poweron", "ok");
		    setElemColors("b_poweroff", "");
		} else {
		    setElemColors("b_poweron", "");
		    setElemColors("b_poweroff", "error");
		}
	// Блокировка питания (концевик)
		if (st.isPowerLocked) {
		    setElemColors("b_powerunlock", "error");
		} else if (st.isPowerUnlocked) {
		    setElemColors("b_powerunlock", "warning");
		} else {
		    setElemColors("b_powerunlock", "");
		}
	// Питание аппаратуры (низковольтное)
		if (st.isEqPowerOn) {
		    setElemColors("b_eqpoweron", "ok");
		    setElemColors("b_eqpoweroff", "");
		} else {
		    setElemColors("b_eqpoweron", "");
		    setElemColors("b_eqpoweroff", "error");
		}
		if (st.isEqReset) {
		    setElemColors("b_eqreset", "warning");
		} else {
		    setElemColors("b_eqreset", "");
		}
	// Свет малый
		if (st.isLightOn) {
		    if (st.isClosed) {
			setElemColors("b_backlighton", "ok");
		    } else {
			setElemColors("b_backlighton", "warning");
		    }
		    setElemColors("b_backlightoff", "");
		} else {
		    setElemColors("b_backlighton", "");
		    setElemColors("b_backlightoff", "ok");//ok");
		}
	// Свет большой
		if (st.isMainLightOn) {
		    if (st.isClosed) {
			setElemColors("b_lighton", "ok");
		    } else {
			setElemColors("b_lighton", "error");
		    }
		    setElemColors("b_lightoff", "");
		} else {
		    setElemColors("b_lighton", "");
		    setElemColors("b_lightoff", "ok");//ok");
		}
	// Датчик дождя. Он бывает Дождь/НеДождь и Работает/Отключен в любых комбинациях
		if (st.isRain) {
		    if (st.isRainDisabled) {
			setElemColors("b_raindis", "warning");
			setElemColors("b_rainen", "warning");
		    } else {
			setElemColors("b_raindis", "");
			setElemColors("b_rainen", "error");
		    }
		    document.getElementById("error-rain").classList.add("errors__flash_active");
		} else {
		    if (st.isRainDisabled) {
			setElemColors("b_raindis", "error");
			setElemColors("b_rainen", "");
		    } else {
			setElemColors("b_raindis", "");
			setElemColors("b_rainen", "ok");
		    }
		    document.getElementById("error-rain").classList.remove("errors__flash_active");
		}
	// Лампочки ошибок
		roof_err_220_lamp = document.getElementById("error-220");
		if (st.isInputPowerOn) {
		    roof_err_220_lamp.classList.add("errors__flash_yellow");
		    roof_err_220_lamp.classList.remove("errors__flash_active");
		} else {
		    roof_err_220_lamp.classList.add("errors__flash_active");
		    roof_err_220_lamp.classList.remove("errors__flash_yellow");
		}
		roof_err_bat_lamp = document.getElementById("error-battery");
		if (st.isBatteryLow) {
		    roof_err_bat_lamp.classList.add("errors__flash_active");
		} else {
		    roof_err_bat_lamp.classList.remove("errors__flash_active");
		}
		roof_err_tel_up_lamp = document.getElementById("error-tel-up");
		if (st.isTelescopeSafe) {
		    roof_err_tel_up_lamp.classList.remove("errors__flash_active");
		} else {
		    roof_err_tel_up_lamp.classList.add("errors__flash_active");
		}
		roof_err_limit_lamp = document.getElementById("error-limit");
		if (st.isLimit) {
		    roof_err_limit_lamp.classList.add("errors__flash_active");
		} else {
		    roof_err_limit_lamp.classList.remove("errors__flash_active");
		}
		roof_err_emerg_close_lamp = document.getElementById("error-emerg-close");
		if ((st.errorCode & 0x04) != 0) { //isEmergClose) {
		    roof_err_emerg_close_lamp.classList.add("errors__flash_active");
		} else {
		    roof_err_emerg_close_lamp.classList.remove("errors__flash_active");
		}
		roof_err_man_off_lamp = document.getElementById("error-manual-off");
		if ((st.errorCode & 0x01) != 0) { //isManualOff) {
		    roof_err_man_off_lamp.classList.add("errors__flash_active");
		} else {
		    roof_err_man_off_lamp.classList.remove("errors__flash_active");
		}
		roof_err_laser_lamp = document.getElementById("error-laser");
		if ((st.errorCode & 0x08) != 0) { //isLaserErr) {
		    roof_err_laser_lamp.classList.add("errors__flash_active");
		} else {
		    roof_err_laser_lamp.classList.remove("errors__flash_active");
		}
        } catch (e) { logException(e) }
    } // roof_status*.error_code == 0
	else {
		setStyleDisabled("block_dome");
		setStyleDisabled("block_dome_status");
	}
}


// Послать команду контроллеру крыши (см. хелп АСКОМА)
function roofCommand(s) {
    var roof_command_executor = new AlpacaRequest(dome_api_url);
    roof_command_executor.onload = function(a) {
	roof_cmd_status = incServerLinkCounters(a);
	fDisplayUpdate = true;
    }
    roof_command_executor.command(s);
    //serverSendCount++;
    return false;
}
// Послать команду контроллеру крыши через сервисный интерфейс. см. исходники, RoofServiceController.cs
function roofServiceCommand(s) {
    var roof_command_executor = new AlpacaRequest(dome_service_url);
    roof_command_executor.onload = function(a) {
	roof_cmd_status = incServerLinkCounters(a);
	fDisplayUpdate = true;
    }
    roof_command_executor.command(s);
    //serverSendCount++;
    return false;
}

